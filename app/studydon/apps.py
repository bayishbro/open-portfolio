from django.apps import AppConfig


class StudydonConfig(AppConfig):
    name = 'studydon'
