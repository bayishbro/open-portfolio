from django.db import models
import uuid
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from rest_framework_simplejwt.tokens import RefreshToken
from datetime import timedelta, datetime


def case_upload_location():
    return []


def jsonfield_default_value():
    return {}


def my_upload_function(instance, filename):
    ext = filename.split('.')[-1]
    nameFile = f'{uuid.uuid4()}.{ext}'
    return nameFile


class AllFile(models.Model):
    file = models.FileField(upload_to=my_upload_function, blank=True)


class Draw(models.Model):
    character = models.CharField(blank=True, max_length=100)
    strokes = ArrayField(models.TextField(), default=case_upload_location)
    medians = models.TextField(blank=True)


class Lesson(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, max_length=100)
    date_created = models.DateTimeField(("Created"), default=datetime.now)
    index = models.IntegerField(default=0)


class Chapter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, max_length=100)
    lesson = models.ForeignKey(
        'Lesson', on_delete=models.CASCADE, related_name='lesson_chapters')
    date_created = models.DateTimeField(("Created"), default=datetime.now)
    index = models.IntegerField(default=0)


class Sub_lesson(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    image = models.JSONField(default=jsonfield_default_value)
    description = models.CharField(blank=True, max_length=100)
    chapter = models.ForeignKey(
        'Chapter', on_delete=models.CASCADE, related_name='chapter_sub_lessons')
    date_created = models.DateTimeField(("Created"), default=datetime.now)
    index = models.IntegerField(default=0)


class Step(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(blank=True, max_length=100)
    params = models.JSONField(blank=True)
    sublesson = models.ForeignKey(
        'Sub_lesson', on_delete=models.CASCADE, related_name='sub_lesson_steps')
    date_created = models.DateTimeField(("Created"), default=datetime.now)
    index = models.IntegerField(default=0)

    def __str__(self):
        return self.type


class AnimationGroup(models.Model):
    preview_pic_name = models.CharField(max_length=150, blank=True)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)
    subscribe = models.BooleanField(default=False)


class Animation(models.Model):
    name = models.CharField(max_length=300, blank=False)
    preview_pic_name = models.CharField(max_length=150, blank=True)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)
    hieroglyph = models.CharField(max_length=200, blank=True)
    color = models.CharField(max_length=200, default='0xFFFFCD00')
    video_url = models.URLField(blank=True)
    top = models.BooleanField(default=False)
    subscribe = models.BooleanField(default=False)
    animation_group_fk = models.ForeignKey(
        'AnimationGroup', on_delete=models.CASCADE, related_name='animtion_group_url', default=True)

    def __str__(self):
        return self.name


class Comic(models.Model):
    name = models.CharField(max_length=300, blank=False)
    preview_pic_name = models.CharField(max_length=150, blank=False)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)
    subscribe = models.BooleanField(default=False)


class ComicsUrl(models.Model):
    preview_pic_name = models.CharField(max_length=150, blank=False)
    pics_urls = models.FileField(upload_to=my_upload_function, blank=True)
    comics_urls = models.ForeignKey(
        'Comic', on_delete=models.CASCADE, related_name='comics_urls')


class VideoGroup(models.Model):
    color = models.CharField(max_length=200, default='0xFFFFCD00')
    subscribe = models.BooleanField(default=False)


class Video(models.Model):
    name = models.CharField(max_length=150, blank=False)
    preview_pic_name = models.CharField(max_length=150, blank=False)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)
    video_url = models.URLField(blank=False)
    top = models.BooleanField(default=False)
    subscribe = models.BooleanField(default=False)
    video_group_fk = models.ForeignKey(
        'VideoGroup', on_delete=models.CASCADE, related_name='video_group_url',  default=True)


class Podcast(models.Model):
    name = models.CharField(max_length=150, blank=False)
    description = models.TextField(blank=False)
    preview_pic_name = models.CharField(max_length=150, blank=False)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)


class PodcastVideo(models.Model):
    name = models.CharField(max_length=150, blank=False)
    preview_pic_name = models.CharField(max_length=150, blank=False)
    preview_pic_url = models.FileField(
        upload_to=my_upload_function, blank=True)
    video_url = models.URLField(blank=False)
    subscribe = models.BooleanField(default=False)
    podcast_fk = models.ForeignKey(
        'Podcast', on_delete=models.CASCADE, related_name='podcast_url')


class UserManager(BaseUserManager):

    def create_user(self, name, email, password=None):
        if email is None:
            raise TypeError('Users should have a Email')

        user = self.model(email=self.normalize_email(email))
        user.name = name
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None):
        if password is None:
            raise TypeError('Password should not be none')

        user = self.create_user(email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


# AUTH_PROVIDERS = {'facebook': 'facebook', 'google': 'google','twitter': 'twitter', 'email': 'email'}


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    name = models.CharField(blank=True, max_length=50)
    reset_password_code = models.IntegerField(default=0, unique=False,)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }
