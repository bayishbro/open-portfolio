from django.contrib import admin

from .models import *


# Register your models here.

class RewardTabularInline(admin.TabularInline):
    model = ComicsUrl
    extra = 1


class EndlessAdmin(admin.ModelAdmin):
    inlines = [RewardTabularInline]

    class Meta:
        model = Comic


class PodcastTabularInline(admin.TabularInline):
    model = PodcastVideo
    extra = 1


class PodcastAdmin(admin.ModelAdmin):
    inlines = [PodcastTabularInline]

    class Meta:
        model = Podcast


class VideoTabularInline(admin.TabularInline):
    model = Video
    extra = 1


class VideoAdmin(admin.ModelAdmin):
    inlines = [VideoTabularInline]

    class Meta:
        model = VideoGroup


class AnimationTabularInline(admin.TabularInline):
    model = Animation
    extra = 1


class AnimationAdmin(admin.ModelAdmin):
    inlines = [AnimationTabularInline]

    class Meta:
        model = AnimationGroup


admin.site.register(AllFile)
admin.site.register(Lesson)
admin.site.register(Draw)
admin.site.register(Chapter)
admin.site.register(Sub_lesson)
admin.site.register(Step)
admin.site.register(User)
admin.site.register(AnimationGroup, AnimationAdmin)
admin.site.register(Comic, EndlessAdmin)
admin.site.register(VideoGroup, VideoAdmin)
admin.site.register(Podcast, PodcastAdmin)
