from studydon.models import Comic, ComicsUrl
from rest_framework import serializers


class ComicsUrlsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComicsUrl
        fields = ['preview_pic_name', 'pics_urls']


class ComicsSerializer(serializers.HyperlinkedModelSerializer):
    pics_urls = serializers.SerializerMethodField()

    def get_pics_urls(self, obj):
        enemy_data = ComicsUrlsSerializer(
            obj.comics_urls.all().order_by('id'), many=True).data
        return enemy_data

    class Meta:
        model = Comic
        fields = ['name', 'preview_pic_name',
                  'preview_pic_url', 'subscribe', 'pics_urls']
