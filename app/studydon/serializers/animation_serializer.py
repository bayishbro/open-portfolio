from studydon.models import Animation, AnimationGroup
from rest_framework import serializers


class AnimationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Animation
        fields = ['name', 'hieroglyph', 'color', 'preview_pic_url',
                  'preview_pic_name', 'video_url', 'top', 'subscribe']


class AnimationGroupSerializer(serializers.ModelSerializer):
    animations = serializers.SerializerMethodField()

    def get_animations(self, obj):
        animation_data = AnimationSerializer(
            obj.animtion_group_url.all().order_by('id'), many=True).data
        return animation_data

    class Meta:
        model = AnimationGroup
        fields = ['id', 'subscribe', 'preview_pic_name',
                  'preview_pic_url', 'animations']
