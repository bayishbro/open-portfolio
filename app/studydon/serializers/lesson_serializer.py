from rest_framework import permissions, routers, serializers, viewsets
from studydon.models import *


class AllfileSerialize(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AllFile
        fields = ['file']


class DrawSerialize(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Draw
        fields = ['id', 'character', 'strokes', 'medians']


class StepSerializer(serializers.ModelSerializer):

    class Meta:
        model = Step
        fields = ['id', 'type', 'params', 'sublesson', 'date_created', 'index']


class Sub_lessonSerializer(serializers.ModelSerializer):
    steps = serializers.SerializerMethodField()

    def get_steps(self, obj):
        return StepSerializer(obj.sub_lesson_steps.all().order_by('index'), many=True).data

    class Meta:
        model = Sub_lesson
        fields = ['id', 'image', 'description',
                  'steps', 'chapter', 'date_created', 'index']


class ChapterSerializer(serializers.ModelSerializer):
    sub_lessons = serializers.SerializerMethodField()

    def get_sub_lessons(self, obj):
        return Sub_lessonSerializer(obj.chapter_sub_lessons.all().order_by('index'), many=True).data

    class Meta:
        model = Chapter
        fields = ['id', 'name', 'sub_lessons',
                  'lesson', 'date_created', 'index']


class LessonSerializer(serializers.ModelSerializer):
    chapters = serializers.SerializerMethodField()

    def get_chapters(self, obj):
        return ChapterSerializer(obj.lesson_chapters.all().order_by('index'), many=True).data

    class Meta:
        model = Lesson
        fields = ['id', 'chapters', 'name', 'date_created', 'index']
