from studydon.models import Video, VideoGroup
from rest_framework import serializers


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields = ['name', 'preview_pic_name',
                  'preview_pic_url', 'video_url', 'top', 'subscribe']


class VideoGroupSerializer(serializers.ModelSerializer):
    videos = serializers.SerializerMethodField()

    def get_videos(self, obj):
        enemy_data = VideoSerializer(
            obj.video_group_url.all().order_by('id'), many=True).data
        return enemy_data

    class Meta:
        model = VideoGroup
        fields = ['id', 'color', 'subscribe', 'videos']
