from studydon.models import Podcast, PodcastVideo
from rest_framework import serializers


class PodcastsFKSerializer(serializers.ModelSerializer):
    class Meta:
        model = PodcastVideo
        fields = ['name', 'preview_pic_name',
                  'preview_pic_url', 'video_url', 'subscribe']


class PodcastsSerializer(serializers.HyperlinkedModelSerializer):
    videos = serializers.SerializerMethodField()

    def get_videos(self, obj):
        podcast_data = PodcastsFKSerializer(
            obj.podcast_url.all().order_by('id'), many=True).data
        return podcast_data

    class Meta:
        model = Podcast
        fields = ['name', 'description',
                  'preview_pic_name', 'preview_pic_url', 'videos']
