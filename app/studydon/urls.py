from django.conf.urls import url
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.urls import path, include
from rest_framework import permissions, routers, serializers, viewsets
from django.contrib.auth.models import User
from studydon.controllers.user_controller import *
from studydon.controllers.lesson_controller import *
from studydon.controllers.animation_controller import *
from studydon.controllers.comics_controller import *
from studydon.controllers.video_controller import *
from studydon.controllers.podcast_controller import *

from studydon import views

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, routers, serializers, viewsets

schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version='v1',
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="bayishbro@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = routers.DefaultRouter()
router.register('lesson', LessonViewSet)
router.register('chapter', ChapterViewSet)
router.register('sub-lesson', Sub_lessonViewSet)
router.register('step', StepViewSet)
router.register('animations-groups', AnimationSet)
router.register('top-animation', TopAnimationSet,  basename='topAnimation')
router.register('video-groups', VideoSet)
router.register('top-video', TopVideoSet,  basename='topVideo')
router.register('comics', ComicsSet)
router.register('podcasts', PodcastSet)


urlpatterns = [
    url('swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(
        cache_timeout=0), name='schema-json'),
    url('swagger/$', schema_view.with_ui('swagger',
                                         cache_timeout=0), name='schema-swagger-ui'),
    url('redoc/$', schema_view.with_ui('redoc',
                                       cache_timeout=0), name='schema-redoc'),
    url('', include(router.urls)),

    url('upload_file', LessonController.upload_file, name='upload_file'),
    url('delete_file', LessonController.delete_file, name='delete_file'),


    url('register/', RegisterView.as_view(), name='register'),
    url('login/', LoginAPIView.as_view(), name='login'),
    url('email-verify/', VerifyEmail.as_view(), name="email-verify"),
    url('token/',  TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url('getToken/refresh/',  TokenRefreshView.as_view(), name='token_refresh'),
    path('request-reset-email/', RequestPasswordResetEmail.as_view(),
         name="request-reset-email"),
    path('password-reset/<uidb64>/<token>/',
         PasswordTokenCheckAPI.as_view(), name='password-reset-confirm'),
    path('password-reset-complete/', SetNewPasswordAPIView.as_view(),
         name='password-reset-complete')

]
