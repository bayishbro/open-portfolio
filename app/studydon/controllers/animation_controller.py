from studydon.serializers.animation_serializer import *
from studydon.models import Animation, AnimationGroup
from rest_framework import viewsets
from rest_framework.response import Response


class AnimationSet(viewsets.ModelViewSet):
    queryset = AnimationGroup.objects.all().order_by('id')
    serializer_class = AnimationGroupSerializer

    def list(self, request):
        queryset = AnimationGroup.objects.order_by('id')
        serializer = AnimationGroupSerializer(queryset, many=True)
        return Response(serializer.data)


class TopAnimationSet(viewsets.ViewSet):
    def list(self, request):
        animation = Animation.objects.filter(top=True).order_by('id')
        serializer = AnimationSerializer(animation, many=True)
        return Response(serializer.data)
