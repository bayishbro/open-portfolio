import os
import json
from uuid import UUID, uuid4
import uuid
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from rest_framework import viewsets
from studydon.serializers.lesson_serializer import *
from studydon.models import *
from hello_django.settings import dbx


class LessonController(object):

    def upload_file(request):
        try:
            if request.method == 'POST':
                files_list = request.FILES
                for file in files_list:
                    new_file = AllFile.objects.create(file=files_list[file])
                    new_file.save()

                data3 = dbx.sharing_create_shared_link(new_file.file.name)
                data = {
                    "original_name": files_list[file].name,
                    "name": new_file.file.name,
                    "url": data3.url + '&raw=1'
                }
                return JsonResponse(
                    data,
                    status=200)
            else:
                return HttpResponse()
        except Exception as e:
            print(e)
            return HttpResponse(e)

    def delete_file(request):
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            file_instance = AllFile.objects.get(filename=body['file'])
            if file_instance.filename:
                logo = file_instance.filename
                if logo.file:
                    if os.path.isfile(logo.path):
                        logo.file.close()
                        os.remove(logo.path)
            file_instance.delete()
            return HttpResponse("DELETED")
        except Exception as e:
            print(e)
            return HttpResponse({'ERROR'}, status=400)


class AllfileSet(viewsets.ModelViewSet):
    queryset = AllFile.objects.all()
    serializer_class = AllfileSerialize


class LessonSet(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer


class DrawSet(viewsets.ModelViewSet):
    queryset = Draw.objects.all()
    serializer_class = DrawSerialize


class LessonViewSet(viewsets.ModelViewSet):
    serializer_class = LessonSerializer
    queryset = Lesson.objects.order_by('index')


class ChapterViewSet(viewsets.ModelViewSet):
    serializer_class = ChapterSerializer
    queryset = Chapter.objects.order_by('index')


class Sub_lessonViewSet(viewsets.ModelViewSet):
    serializer_class = Sub_lessonSerializer
    queryset = Sub_lesson.objects.order_by('index')


class StepViewSet(viewsets.ModelViewSet):
    serializer_class = StepSerializer
    queryset = Step.objects.all().order_by('index')
