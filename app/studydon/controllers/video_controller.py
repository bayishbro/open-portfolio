from studydon.serializers.video_serializer import *
from studydon.models import Video, VideoGroup
from rest_framework import viewsets
from rest_framework.response import Response


class VideoSet(viewsets.ModelViewSet):
    queryset = VideoGroup.objects.all().order_by('id')
    serializer_class = VideoGroupSerializer

    def list(self, request):
        queryset = VideoGroup.objects.order_by('id')
        serializer = VideoGroupSerializer(queryset, many=True)
        return Response(serializer.data)


class TopVideoSet(viewsets.ViewSet):
    def list(self, request):
        inbox = Video.objects.filter(top=True).order_by('id')
        serializer = VideoSerializer(inbox, many=True)
        return Response(serializer.data)
