from studydon.serializers.podcast_serializer import *
from studydon.models import Podcast
from rest_framework import viewsets
from rest_framework.response import Response


class PodcastSet(viewsets.ModelViewSet):
    queryset = Podcast.objects.all().order_by('id')
    serializer_class = PodcastsSerializer

    def list(self, request):
        queryset = Podcast.objects.order_by('id')
        serializer = PodcastsSerializer(queryset, many=True)
        return Response({"podcasts": serializer.data})
