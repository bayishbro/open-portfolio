from studydon.serializers.comics_serializer import *
from studydon.models import Comic
from rest_framework import viewsets
from rest_framework.response import Response


class ComicsSet(viewsets.ModelViewSet):
    queryset = Comic.objects.all().order_by('id')
    serializer_class = ComicsSerializer

    def list(self, request):
        queryset = Comic.objects.order_by('id')
        serializer = ComicsSerializer(queryset, many=True)
        comics_data = {
            "comics": serializer.data
        }
        return Response(comics_data)
